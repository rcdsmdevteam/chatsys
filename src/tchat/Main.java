package tchat;
	
import java.io.InputStream;

import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.application.Application;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;
import javafx.util.Duration;


public class Main extends Application {
	
	public static Stage pS;
	public static String version = "0.2";
	Image R;
	Image RJaune;
	@Override
	public void start(final Stage primaryStage) {
		try {
			R = new Image(Main.class.getResourceAsStream("R.jpg"));
			RJaune = new Image(Main.class.getResourceAsStream("RJaune.jpg"));
			BorderPane root = new BorderPane();
			FXMLLoader loader = new FXMLLoader(Main.class.getResource("Connexion.fxml"));
            AnchorPane anchor = (AnchorPane) loader.load();
            ControleurLoggin Controller = (ControleurLoggin) loader.getController();
            Scene scene = new Scene(anchor);
			primaryStage.setScene(scene);
			primaryStage.show();
			primaryStage.getIcons().add(R);
			primaryStage.setTitle("Chat RCDSM 2014-2015");
			pS = primaryStage;

			Timeline Tline = new Timeline(new KeyFrame(Duration.millis(1000), new EventHandler<ActionEvent>() {
			    @Override
			    public void handle(ActionEvent event) {
						if (primaryStage.getIcons().contains(RJaune)){
							primaryStage.getIcons().remove(RJaune);
							primaryStage.getIcons().add(R);
						}else
						if (primaryStage.getIcons().contains(R)){
							primaryStage.getIcons().remove(R);
							primaryStage.getIcons().add(RJaune);
						}
			    }
			}));
			
			Tline.setCycleCount(Timeline.INDEFINITE);
			Tline.play();
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	public static void main(String[] args) {
		launch(args);
	}
}
