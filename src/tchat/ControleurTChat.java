package tchat;


import java.awt.Desktop;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;
import java.util.ConcurrentModificationException;
import java.util.HashMap;
import java.util.Map.Entry;
import java.util.TreeMap;

import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.concurrent.Task;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.Cursor;
import javafx.scene.control.ColorPicker;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.scene.text.TextFlow;
import javafx.stage.WindowEvent;
import javafx.util.Duration;
import fr.orandjadev.MJson.JsonArray;
import fr.orandjadev.MJson.JsonObject;
import fr.orandjadev.MJson.JsonReader;

public class ControleurTChat {
	
	//module d'appelau serveur
	HttpGet conn = new HttpGet();
	//la ou l'on écrit.
	@FXML TextField writer;
	
	//Sert a aligner les TextFlow dans le chat
	//tableau a 1 colone 
	@FXML VBox vbox;
	
	String name;
	String pass;
	
	//si jamais c'est long...
	@FXML ProgressIndicator progress;
	//
	@FXML ProgressIndicator colorprogress;
	
	@FXML
	ColorPicker colorpicker;
	
	@FXML
	ScrollPane sp;
	
	TreeMap<Integer,TextFlow> Messages = new TreeMap<Integer,TextFlow>();
	HashMap<Integer,Boolean> preMessages = new HashMap<Integer,Boolean>();
	HashMap<String,Color> chatterscolors = new HashMap<String,Color>();
	
	ArrayList<TextFlow> msgSupplementaire = new ArrayList<TextFlow>();

	boolean removechattextbox=false;
	boolean lockenter =false;
	long time;
	boolean stopgetmessage = true;
	Runnable r = new Runnable(){
		public void run() {
			while (stopgetmessage){
				try {
					time = System.currentTimeMillis();
					System.out.print("get : ");
					GetMsg();
					System.out.println((System.currentTimeMillis()-time) + "ms elsapsed");
					Thread.sleep(1000);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		}
	};
	Thread getAsyncMsg = new Thread(r);
	
	Timeline Tline = new Timeline(new KeyFrame(Duration.millis(500), new EventHandler<ActionEvent>() {
	    @Override
	    public void handle(ActionEvent event) {
			refreshchat();
			Thread.yield();
	    }
	}));
	
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@FXML
	public void initialize(){
		
		vbox.setSpacing(5);
		writer.setOnKeyPressed(new EventHandler<KeyEvent>(){
			        @Override
			        public void handle(KeyEvent ke){
			            if (ke.getCode().equals(KeyCode.ENTER) && !lockenter){
			            	System.out.println(writer.getStyleClass());
			            	String s = writer.getText();
			            	if(!s.equals("")){
				            	lockenter = true;
				        		writer.getStyleClass().add("grey");
				            	writer.editableProperty().set(false);
			            		if(iscommand(s)){
			            			DoCommand(s);
			            		}else SendMsg(s);
			            	}
			            }
			        }
			    });
		
		vbox.heightProperty().addListener(new ChangeListener() {
			public void changed(ObservableValue observable, Object oldValue, Object newValue) {
				sp.setVvalue(sp.getVmax());
			};
		});
		Main.pS.setOnCloseRequest(new EventHandler<WindowEvent>() {
		    @Override
		    public void handle(WindowEvent event) {
		    	stopgetmessage = false;
		    }
		});
	}
	
	public void refreshchat(){
		for(Entry<Integer, TextFlow> entry : Messages.entrySet()) {
			  Integer key = entry.getKey();
			  TextFlow value = entry.getValue();
			  if(preMessages.containsKey(key)){
				  if(!preMessages.get(key)){
					  PutMsgOnScreen(value);
					 //Main.pS.toFront();
				  }
			  }else{
				  preMessages.put(key, true);
				  PutMsgOnScreen(value);
				  //Main.pS.toFront();
			  }
			}
		try{
			synchronized(msgSupplementaire){
				for(TextFlow t : msgSupplementaire){
					PutMsgOnScreen(t);
					//Main.pS.toFront();
				}
				msgSupplementaire.clear();
			}
		}catch(ConcurrentModificationException e){
		}
		if (!getAsyncMsg.isAlive()){
			getAsyncMsg = new Thread(r);
			getAsyncMsg.start();
		}
		if(removechattextbox){
			writer.setText("");
			removechattextbox = false;
    		writer.editableProperty().set(true);
    		writer.getStyleClass().remove("grey");
    		lockenter = false;
    	}
		if(System.currentTimeMillis()-time > 3000) progress.visibleProperty().set(true);
		else progress.visibleProperty().set(false);
		//System.out.print(".");
	}
	
	public void PutMsgOnScreen(String name , String msg){
		JsonObject obj = new JsonObject();
		obj.put("auteur", name);
		obj.put("contenu", msg);
		obj.put("destinataire", "");
		PutMsgOnScreen(GetMessageTextFlow(obj));
	}
	
	public void PutErrOnScreen(String Err){
		Text err = new Text();
		err.setText(Err);
		err.setFont(Font.font("Verdana", FontWeight.BOLD, 12));
		err.setFill(Color.RED);
		
		msgSupplementaire.add(new TextFlow(err));
	}
	public void PutUsersMessagesOnScreen(JsonArray posts){
		for (int i = 0; i < posts.size(); i++) {
			JsonObject post = (JsonObject) posts.get(Integer.toString(i));
			
			//System.out.println(obj);
			Messages.put(new Integer((String)post.get("id"))
					,GetMessageTextFlow(post)
					);
		}
	}
	
	public void PutMsgOnScreen(TextFlow flow){
		flow.setPrefWidth(vbox.getWidth());
		vbox.getChildren().add(flow);
	}
	
	
	public void SendMsg(final String message){
		new Thread(){
			public void run() {
				String result = conn.sendMsg(name, pass, message);
				String [] s = result.split(";",3);
				//System.out.println(result);
				if(s[0].equals("OK")){
					JsonObject obj = new JsonObject();
					obj.put("auteur", name);
					obj.put("contenu", writer.getText());
					obj.put("destinataire", "");
					Messages.put(Integer.valueOf(s[1]), GetMessageTextFlow(obj));
					removechattextbox = true;
				}
				else{
					PutErrOnScreen(result);
				}
			}
		}.start();
		
	}
	
	/**
	 * Link.
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public Text[] parseMsg(String Msg){
		String [] split = Msg.split(" ");
		ArrayList<Text> texts = new ArrayList<Text>();
		Text t;
		for (int i = 0; i < split.length; i++) {
			
			//System.out.println(split[i]);
			try {
			    final URL url = new URL(split[i]);
				t = new Text(split[i]);
				t.setFill(Color.BLUE);
				t.onMouseEnteredProperty().set(new EventHandler(){
					@Override
					public void handle(Event arg0) {
						sp.setCursor(Cursor.HAND);
					}
				});
				t.onMouseExitedProperty().set(new EventHandler(){
					@Override
					public void handle(Event arg0) {
						sp.setCursor(Cursor.DEFAULT);
					}
				});
				t.setOnMouseClicked(new EventHandler<Event>() {
					@Override
					public void handle(Event arg0) {
						try {
							openWebpage(url.toURI());
						} catch (URISyntaxException e) {
							e.printStackTrace();
						}
					}
				});
				texts.add(t);
			} catch (MalformedURLException e) {
				texts.add(new Text(split[i]));
			}
			texts.add(new Text(" "));
		}
		return texts.toArray(new Text[]{});
	}
	
	public void ResizeWidth(double width){
		synchronized (vbox) {
			vbox.setPrefWidth(width-40);
		}
	}
	
	public String GetMsg(){
		String m = conn.getMsg(name, pass);
		String[]s = m.split(";",2);
		if(!s[0].equals("Erreur")){
			JsonObject messages = JsonReader.ReadJson(m);
			PutUsersMessagesOnScreen((JsonArray) messages.get("messages"));
		}
		return null;
	}
	
	public TextFlow GetMessageTextFlow(JsonObject Message){
		ArrayList<Text> texts = new ArrayList<Text>();
		String pseudo = (String) Message.get("auteur");
		Text user = new Text();
		if(!Message.get("destinataire").equals("")){
			texts.add(new Text("MP "));
		}
		user.setText(pseudo + " : ");
		user.setFont(Font.font("Verdana", FontWeight.BOLD, 12));
		user.setFill(GetColor(pseudo));
		Text[] Tmsg = parseMsg(Message.get("contenu").toString());
		
		texts.add(user);
		for (int i = 0; i < Tmsg.length; i++) {
			//System.out.println(Tmsg[i]);
			texts.add(Tmsg[i]);
		}
		TextFlow tf = new TextFlow();
		tf.getChildren().addAll(texts);
		return tf;
	}

	public Color GetColor(String user){
		if(chatterscolors.containsKey(user)){
			return chatterscolors.get(user);
		}
		String userInfo = conn.getInfo(user);
		String[] s = userInfo.split(";");
		if (!s[0].equals("Erreur")){
			String color = (String) JsonReader.ReadJson(userInfo).get("infos","0","couleur");
			chatterscolors.put(user, Color.valueOf(color));
			return chatterscolors.get(user);
		}
		chatterscolors.put(user, Color.valueOf("000000"));
		return chatterscolors.get(user);
		
	}
	
	public void openWebpage(URI uri) {
	    Desktop desktop = Desktop.isDesktopSupported() ? Desktop.getDesktop() : null;
	    if (desktop != null && desktop.isSupported(Desktop.Action.BROWSE)) {
	        try {
	            desktop.browse(uri);
	        } catch (Exception e) {
	            e.printStackTrace();
	        }
	    }
	}
	
	public void setUp(JsonObject messages,JsonObject infoUsers){
		this.name = (String) infoUsers.get("user");
		this.pass = (String) infoUsers.get("pass");
		
		PutUsersMessagesOnScreen((JsonArray)messages.get("messages"));
		setInfos(infoUsers);
		colorpicker.setValue(chatterscolors.get(name));
		colorpicker.setPromptText(chatterscolors.get(name).toString());
		getAsyncMsg.start();
		
		if(!Main.version.equals(infoUsers.get("version"))){
			new Thread(new Task(){
				protected Object call() throws Exception {
					Thread.sleep(10000);
					Text version = new Text("Nouvelle version Disponible!!");
					version.setFont(Font.font("Verdana", FontWeight.BOLD, 12));
					version.setFill(Color.GREEN);
					synchronized(msgSupplementaire){
						msgSupplementaire.add(new TextFlow(version));
						msgSupplementaire.add(new TextFlow(parseMsg("http://lopma.com/rcdsm/chatsys/jar/chatrcdsm.jar")));
					}
					return null;
				}
			}).start();
		}
		
		Tline.setCycleCount(Timeline.INDEFINITE);
		Tline.play();
	}
	
	public void setInfos(JsonObject infoUsers){
		JsonArray users = (JsonArray) infoUsers.get("infos");
		for (int i = 0; i < users.size(); i++) {
			chatterscolors.put(users.get(String.valueOf(i),"pseudo").toString(), Color.valueOf(users.get(String.valueOf(i),"couleur").toString()));
		}
		
	}
	
	public boolean iscommand(String Msg){
		if (Msg.charAt(0)=='/'){
			return true;
		}
		return false;
	}
	
	public void DoCommand(String Msg){
		String command = Msg.split(" ",2)[0];
		if(command.equals("/pass")){
			String[] cmd = Msg.split(" ");
			final String pass = cmd[1];
			final String newpass = cmd[2];
			if(this.pass.equals(pass)){
				new Thread(){
					public void run() {
						String[] rep = conn.changepassword(name, pass, newpass).split(";");
						if(!rep[0].equals("Erreur")){
							synchronized(msgSupplementaire){
								Text t = new Text(rep[2]);
								t.setFill(Color.GREEN);
								msgSupplementaire.add(new TextFlow(t));
								removechattextbox = true;
							}
						}
					};
				}.start();
			}
			else{
				synchronized(msgSupplementaire){
					PutErrOnScreen("Mauvais ancien mot de passe");
				}
			}
		}
		
		if(command.equals("/pm")){
			String[] Mp = Msg.split(" ",3);
			final String dest = Mp[1];
			final String Mess = Mp[2];
			new Thread(){
				public void run() {
					String[] result = conn.sendMsg(name, pass, Mess, dest).split(";");
					if(result[0].equals("Error")){
						synchronized (msgSupplementaire){
							PutErrOnScreen(result[1]);
						}
					}else removechattextbox = true;
				}
			}.start();
		}
	}
	
	public void changecolor(){
		colorprogress.setVisible(true);
		new Thread(new Task(){
			@Override
			protected Object call() throws Exception {
				String[] m = conn.changecolor(name, pass, colorpicker.valueProperty().get().toString().substring(2, 8)).split(";");
				if (!m[0].equals("Erreur")){
					chatterscolors.put(name, colorpicker.valueProperty().get());
					Text t = new Text(m[2]);
					t.setFill(Color.GREEN);
					msgSupplementaire.add(new TextFlow(t));
				}else{
					Text t = new Text("Erreur...");
					t.setFill(Color.GREEN);
					msgSupplementaire.add(new TextFlow(t));
					colorpicker.valueProperty().set(chatterscolors.get(name));
				}
				
				Platform.runLater(new Runnable(){
					public void run() {
						colorprogress.setVisible(false);	
					}
				});
				
				return null;
			}
		}).start();
	}
	
}
