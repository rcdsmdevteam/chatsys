package tchat;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;


public class HttpGet {
	
	
		// HTTP GET request
		public static String path = "http://lopma.com/rcdsm/chatsys/interfaces";
		public String changecolor(String user,String pass,String couleur){
			try {
				return sendGet(path+"/profileupdate.php?user="+user+"&pass="+MD5(pass)+"&couleur="+couleur);
			} catch (Exception e) {
				e.printStackTrace();
				return null;
			}
		}
		public String changepassword(String user,String pass,String password){
			try {
				return sendGet(path+"/profileupdate.php?user="+user+"&pass="+MD5(pass)+"&newpass="+MD5(password));
			} catch (Exception e) {
				e.printStackTrace();
				return null;
			}
		}
		
		public String sendMsg(String user,String pass,String Msg){
			try {
				return sendPost(path+"/ul.php?user="+user+"&pass="+MD5(pass) , Msg);
			} catch (Exception e) {
				e.printStackTrace();
				return null;
			}
		}
		
		public String sendMsg(String user,String pass,String Msg, String dest){
			try {
				return sendPost(path+"/ul.php?user="+user+"&pass="+MD5(pass)+"&dest="+dest, Msg);
			} catch (Exception e) {
				return null;
			}
		}
		
		public String getMsg(String user,String pass){
			try {
				//return TransformgetMsg(sendGet("http://orandjadev.fr/dl/lol.html"));
				//System.out.println(MD5(pass));
				return TransformgetMsg(sendGet(path+"/dl.php?user="+user+"&pass="+MD5(pass)));
			} catch (Exception e) {
				return null;
			}
		}
		public String getAllMsg(String user,String pass){
			try {
				//return TransformgetMsg(sendGet("http://orandjadev.fr/dl/lol.html"));
				//System.out.println(MD5(pass));
				return TransformgetMsg(sendGet(path+"/dl.php?user="+user+"&pass="+MD5(pass)+"&all=lolilo"));
			} catch (Exception e) {
				return null;
			}
		}
		public String getInfo(){
			try {
				return sendGet(path+"/info.php");
			} catch (Exception e) {
				return null;
			}
		}
		public String getInfo(String user){
			try {
				return sendGet(path+"/info.php?user="+user);
			} catch (Exception e) {
				return null;
			}
		}
		public String getVersion(){
			try {
				return sendGet(path+"/version.php");
			} catch (Exception e) {
				return null;
			}
		}
		
		
		public String sendGet(String url) throws Exception {
			
			URL obj = new URL(url);
			HttpURLConnection con = (HttpURLConnection) obj.openConnection();
			
			// optional default is GET TestPseudo
			con.setRequestMethod("GET");
	 
			int responseCode = con.getResponseCode();
			//System.out.println("\nSending 'GET' request to URL : " + url);
			//System.out.println("Response Code : " + responseCode);
			if (responseCode != 200){
				return "Erreur";
			}
			BufferedReader in = new BufferedReader(
			        new InputStreamReader(con.getInputStream()));
			String inputLine;
			StringBuffer response = new StringBuffer();
	 
			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}
			in.close();

			//System.out.println("Response Code : " + response.toString());
			return response.toString();
		}
		
		private String sendPost(String url,String Msg) throws Exception {
			 
			URL obj = new URL(url);
			HttpURLConnection con = (HttpURLConnection) obj.openConnection();
	 
			//add reuqest header
			con.setRequestMethod("POST");
			con.setRequestProperty("Accept-Language", "en-US,en;q=0.5");
			
			String urlParameters = "text="+Msg+" ";
	 
			// Send post request
			con.setDoOutput(true);
			DataOutputStream wr = new DataOutputStream(con.getOutputStream());
			wr.writeBytes(urlParameters);
			wr.flush();
			wr.close();
	 
			int responseCode = con.getResponseCode();
			//System.out.println("\nSending 'POST' request to URL : " + url);
			System.out.println("Post parameters : " + urlParameters);
			//System.out.println("Response Code : " + responseCode);
			if (responseCode != 200){
				return "Erreur";
			}
			
			BufferedReader in = new BufferedReader(
			        new InputStreamReader(con.getInputStream()));
			String inputLine;
			StringBuffer response = new StringBuffer();
	 
			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}
			in.close();
			
			System.out.println("Response Code : " + response.toString());
			return response.toString();
		}
		//je test les diff�rents char sp�ciaux <>#%{}|\^~[]`;/?:@=&$
		public String TransformMsg(String Msg){
			Msg = Msg.replaceAll("\\%", "%25");
			Msg = Msg.replaceAll(" ", "%20");
			Msg = Msg.replaceAll("\\<", "%3C");
			Msg = Msg.replaceAll("\\>", "%3E");
			Msg = Msg.replaceAll("\\{", "%7B");
			Msg = Msg.replaceAll("\\}", "%7D");
			Msg = Msg.replaceAll("\\|", "%7C");
			Msg = Msg.replaceAll("\\#", "%23");
			Msg = Msg.replaceAll("\\\\", "%5C");
			Msg = Msg.replaceAll("\\^", "%5E");
			Msg = Msg.replaceAll("\\~", "%7E");
			Msg = Msg.replaceAll("\\[", "%5B");
			Msg = Msg.replaceAll("\\]", "%5D");
			Msg = Msg.replaceAll("\\`", "%60");
			Msg = Msg.replaceAll("\\;", "%3B");
			Msg = Msg.replaceAll("\\/", "%2F");
			Msg = Msg.replaceAll("\\?", "%3F");
			Msg = Msg.replaceAll("\\:", "%3A");
			Msg = Msg.replaceAll("\\@", "%40");
			Msg = Msg.replaceAll("\\=", "%3D");
			Msg = Msg.replaceAll("\\&", "%26");
			Msg = Msg.replaceAll("\\$", "%24");
			return Msg;
		}
		public String TransformgetMsg(String Msg){
			Msg = Msg.replaceAll("%20" , " ");
			Msg = Msg.replaceAll("%3C" , "<");
			Msg = Msg.replaceAll("%3E" , ">");
			Msg = Msg.replaceAll("%23" , "#");
			Msg = Msg.replaceAll("%25" , "%");
			Msg = Msg.replaceAll("%7B" , "{");
			Msg = Msg.replaceAll("%7D" , "}");
			Msg = Msg.replaceAll("%7C" , "|");
			Msg = Msg.replaceAll("%5C" , "\\");
			Msg = Msg.replaceAll("%5E" , "^");
			Msg = Msg.replaceAll("%7E" , "~");
			Msg = Msg.replaceAll("%5B" , "[");
			Msg = Msg.replaceAll("%5D" , "]");
			Msg = Msg.replaceAll("%60" , "`");
			Msg = Msg.replaceAll("%3B" , ";");
			Msg = Msg.replaceAll("%2F" , "/");
			Msg = Msg.replaceAll("%3F" , "?");
			Msg = Msg.replaceAll("%3A" , ":");
			Msg = Msg.replaceAll("%40" , "@");
			Msg = Msg.replaceAll("%3D" , "=");
			Msg = Msg.replaceAll("%26" , "&");
			Msg = Msg.replaceAll("%24" , "$");
			return Msg;
		}
		public String MD5(String s){
			String ss="";
		    MessageDigest md;
			try {
				md = MessageDigest.getInstance("MD5");
			
			    md.update(s.getBytes());
	
			    byte byteData[] = md.digest();
	
			    StringBuffer sb = new StringBuffer();
			    for (int i = 0; i < byteData.length; i++)
			        sb.append(Integer.toString((byteData[i] & 0xff) + 0x100, 16).substring(1));
			    ss =sb.toString();
			} catch (NoSuchAlgorithmException e) {
				e.printStackTrace();
			}
			return ss;
		}
}
