package tchat;

import java.io.IOException;
import java.util.concurrent.ExecutorService;

import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.concurrent.Task;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import fr.orandjadev.MJson.JsonObject;
import fr.orandjadev.MJson.JsonReader;

public class ControleurLoggin {
	@FXML
	Button button;
	
	@FXML
	TextField username;
	
	@FXML
	PasswordField password;
	
	@FXML
	Text erreur;
	
	@FXML
	ProgressIndicator progress;
	
	@FXML
	RadioButton online;
	
	@FXML
	RadioButton local;
	
	JsonObject messages;
	JsonObject userdata;
	
	public void initialize(){
		password.setOnKeyPressed(new EventHandler<KeyEvent>(){
	        @Override
	        public void handle(KeyEvent ke){
	            if (ke.getCode().equals(KeyCode.ENTER)){
	            	ButtonClick();
	            }
	        }
	    });
		button.setOnKeyPressed(new EventHandler<KeyEvent>(){
	        @Override
	        public void handle(KeyEvent ke){
	            if (ke.getCode().equals(KeyCode.ENTER)){
	            	ButtonClick();
	            }
	        }
	    });
	}
	Thread start = new Thread();
	
	public void ButtonClick(){
		if(!start.isAlive()){
			progress.setVisible(true);
			start = new Thread(new Task() {
				@Override
				protected Object call() throws Exception {
					String user = username.getText();
					String pass = password.getText();
					HttpGet conn = new HttpGet();
					String rep = conn.getAllMsg(user,pass);
					String infos = conn.getInfo();
					String version = conn.getVersion();
					if (!rep.split(";")[0].equals("Erreur")){
							messages = JsonReader.ReadJson(rep);
							userdata = JsonReader.ReadJson(infos);
							userdata.put("user", user);
							userdata.put("pass", pass);
							userdata.put("version", version);
							Platform.runLater(new Runnable() {
								@Override
								public void run() {
									try {
										OpenTchat();
									} catch (IOException e) {
										// TODO Auto-generated catch block
										e.printStackTrace();
									}	
								}
							});
							Thread.yield();
					}else{
						Platform.runLater(new Runnable() {
							@Override
							public void run() {
								erreur.setText("Erreur Identifiant ou Mot de passe incorect");
								erreur.setFill(Color.RED);
								progress.setVisible(false);
							}
						});
					}
					return null;
				}
			});
			start.start();
		}
	}
	
	private void OpenTchat() throws IOException{
		FXMLLoader loader = new FXMLLoader(Main.class.getResource("View.fxml"));
        AnchorPane anchor = (AnchorPane) loader.load();
        final ControleurTChat Controller = (ControleurTChat) loader.getController();
        Scene scene = new Scene(anchor);
		scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
        scene.widthProperty().addListener(new ChangeListener<Number>() {
            @Override
            public void changed(ObservableValue<? extends Number> observableValue, Number oldSceneWidth, Number newSceneWidth) {
            	Controller.ResizeWidth(newSceneWidth.doubleValue());
            }
        });
        Main.pS.setScene(scene);
        Controller.setUp(messages,userdata);
	}
	
	public void OnLocalClicked(){
		HttpGet.path = "http://serveurlinux/rcdsm/2014/chatsys/interfaces";
		online.setSelected(false);
	}
	
	public void OnOnlineClicked(){
		HttpGet.path = "http://lopma.com/rcdsm/chatsys/interfaces";
		local.setSelected(false);
	}
}
