package fr.orandjadev.MJson;

import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.Map.Entry;

public class JsonObject {
	
	LinkedHashMap<String,JsonObject> childs = new LinkedHashMap<String, JsonObject>();
	LinkedHashMap<String,String> values = new LinkedHashMap<String, String>();
	LinkedHashMap<String,JsonArray> array = new LinkedHashMap<String, JsonArray>();
	
	/**
	 * let's asume the Json Obj is 
	 * { 
	 *   "husband" : { "name":"eric","age":52 },
	 *   "wife" : { "name":"catherine","age":48 },
	 *   "divorced" : false,
	 *   "childs" : [
	 *     {"name":"Deny"} ,
	 *     {"name":"Marc"} ,
	 *     "(something)"
	 *   ]
	 * }
	 * 
	 * path="husband"        return JsonObject { "name":"eric","age":52 }
	 * path="husband*name"   return String eric
	 * path="husband*age"    return JsonNumber 52
	 * path="divorced"       return boolean false
	 * path="childs"         return JsonArray [ {"name":"Deny"} , {"name":"Marc"} , "string (something)" ]
	 * path="childs*0"       return JsonObject {"name":"Deny"}
	 * path="childs*1*name"  return String Marc
	 * path="childs*2"       return String (something)
	 * 
	 * 
	 * you can combine all this. and you can change the "*" separator with the static function.
	 * 
	 */
	
	
	public Object get (String... path_){
		String [] path = Arrays.copyOfRange(path_, 0, path_.length);
		path[0] = "\""+path[0]+"\"";
		if (values.containsKey(path[0])){
			String r = values.get(path[0]);
			if(r.charAt(0)=='"' && r.charAt(r.length()-1)=='"'){
				return r.subSequence(1, r.length()-1);
			}
			else if(JsonNumber.isnumber(r)){
				return new JsonNumber(r);
			}
			else if(r.equals("true")) return true;
			else if(r.equals("false")) return false;
			else return null;
		}
		if (childs.containsKey(path[0])){
			if(path.length>1){
				String[] path2 = Arrays.copyOfRange(path, 1, path.length);
				return childs.get(path[0]).get(path2);
			}
			else return childs.get(path[0]);
		}
		if (array.containsKey(path[0])){
			if(path.length>1){
				String[] path2 = Arrays.copyOfRange(path, 1, path.length);
				return array.get(path[0]).get(path2);
			}
			else return array.get(path[0]);
		}
		return null;
	}
	
	public JsonObject put(String key,JsonObject value){
		key = doKey(key);
		childs.put(key, value);
		return this;
	}
	public JsonObject put(String key,JsonArray value){
		key = doKey(key);
		array.put(key, value);
		return this;
	}
	
	public JsonObject put(String key,String value){
		if (value!=null){
			key = doKey(key);
			values.put(key,"\""+ value +"\"");
		}else put(key);
		return this;
	}
	public JsonObject put(String key,int value){
		key = doKey(key);
		values.put(key, value +"");
		return this;
	}
	public JsonObject put(String key, double value){
		put(key, value, 0);
		return this;
	}
	public JsonObject put(String key, double value, int exponent){
		key = doKey(key);
		values.put(key, new JsonNumber(value, exponent).toString());
		return this;
	}
	public JsonObject put(String key, boolean value){
		key = doKey(key);
		values.put(key, ""+value);
		return this;
	}
	
	public JsonObject put(String key){
		key = doKey(key);
		values.put(key, "null");
		return this;
	}
	
	public JsonObject put(String key,JsonNumber jsonNumber){
		key = doKey(key);
		values.put(key, jsonNumber.toString());
		return this;
	}
	
	public String toString(){
		return toString(0,false);
	}
	public String toString(int decal, boolean structure){
		StringBuilder sb = new StringBuilder();
		boolean inout = false;
		sb.append('{');
		
		//Json values
		for (Entry<String,String> entry : values.entrySet()) {
			inout = true;
			if(structure)sb.append(decal(decal+1));
			if (entry.getValue().charAt(0)=='"'){
				sb.append(entry.getKey()+":"+entry.getValue().substring(1, entry.getValue().length()-1).replaceAll("\\\\", "\\\\\\\\").replaceAll("\"", "\\\\\"") +",");
			}else{
				sb.append(entry.getKey()+":"+entry.getValue() +",");
			}
		}
		
		if(inout && array.size()==0 && childs.size()==0)sb.deleteCharAt(sb.length()-1);
		inout=false;
		
		//json objects
				for (Entry<String,JsonObject> entry : childs.entrySet()) {
					inout = true;
					if(structure)sb.append(decal(decal+1));
					sb.append(entry.getKey()+":");
					sb.append(entry.getValue().toString(decal+1, structure));
					sb.append(',');
				}
		if(inout && array.size()==0)sb.deleteCharAt(sb.length()-1);
		inout=false;
		
		//json Arrays
		for (Entry<String,JsonArray> entry : array.entrySet()) {
			inout = true;
			if(structure)sb.append(decal(decal+1));
			sb.append(entry.getKey()+":");
			sb.append(entry.getValue().toString(decal+1, structure));
			sb.append(',');
		}
		if(inout)sb.deleteCharAt(sb.length()-1);
		inout=false;
		
		if(structure)sb.append(decal(decal));
		sb.append('}');
		return sb.toString();
	}
	
	public String decal(int decal){
		StringBuilder sb = new StringBuilder();
		sb.append('\n');
		for (int i = 0; i < decal; i++) {
			sb.append("    ");
		}
		return sb.toString();
	}
	
	private String doKey(String key){
		key ="\"" + key + "\"";
		removeKey(key);
		return key;
	}
	
	public boolean keyExist(String key){
		return(values.containsKey(key) || childs.containsKey(key) || array.containsKey(key));
	}
	
	public void removeKey(String key){
		if (values.containsKey(key)) values.remove(key);
		if (childs.containsKey(key)) childs.remove(key);
		if (array.containsKey(key)) array.remove(key);
	}
}
