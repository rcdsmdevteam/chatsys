package fr.orandjadev.MJson;

import java.math.BigDecimal;
import java.util.regex.Pattern;

public class JsonNumber {
	double number;
	int exponent;
	
	public static boolean isnumber(String NumberAsString){
		String Regex = "-?(?:0|[1-9]\\d*)(?:\\.\\d+)?(?:[eE][+-]?\\d+)?";
		return NumberAsString.matches(Regex);
	}
	
	public JsonNumber(){}
	public JsonNumber(int number){
		this.number = number;
		this.exponent = 0;
	}
	public JsonNumber(double number){
		this.number = number;
		this.exponent = 0;		
	}
	public JsonNumber(double number,int exponent){
		this.number = number;
		this.exponent = exponent;
	}
	public JsonNumber(String numberAsString){
		String[] split = numberAsString.split("e");
		number = new Double(split[0]);
		if (split.length > 1) exponent = new Integer(split[1]);
		else exponent=0;
	}
	public JsonNumber put(int number){
		this.number = number;
		this.exponent = 0;
		return this;
	}
	public JsonNumber put(double number){
		this.number = number;
		this.exponent = 0;
		return this;
	}
	public JsonNumber put(double number,int exponent){
		this.number = number;
		this.exponent = exponent;
		return this;
	}
	public JsonNumber put(String numberAsString){
		String[] split = numberAsString.split("e");
		number = new Double(split[0]);
		if (split.length > 1) exponent = new Integer(split[1]);
		else exponent=0;
		return this;
	}
	
	public String toString(){
		BigDecimal bvalue = BigDecimal.valueOf(number);
		BigDecimal fractionalPart = bvalue.remainder(BigDecimal.ONE).abs();
		long integralPart  = bvalue.longValue();
		String NumberBuilder = String.valueOf(integralPart);
		if(fractionalPart.doubleValue() != 0){
			NumberBuilder += (""+fractionalPart).substring(1);	
		}
		if (exponent != 0){
			NumberBuilder += "e"+exponent;
		}
		return NumberBuilder;
	}
}
