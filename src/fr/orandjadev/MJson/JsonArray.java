package fr.orandjadev.MJson;

import java.util.ArrayList;
import java.util.Arrays;

@SuppressWarnings("rawtypes")
public class JsonArray {
	ArrayList<Object> elements = new ArrayList<Object>();
	
	public void put(String value){
		elements.add("\""+value+"\"");
	}
	public void put(){
		elements.add("null");
	}
	public void put(boolean b){
		elements.add((b)?"true":"false");
	}
	public void put(int value){
		elements.add(new JsonNumber(value).toString());
	}
	public void put(double value){
		put(value,0);
	}
	public void put(double value, int exponent){
		elements.add(new JsonNumber(value,exponent).toString());
	}
	public void put(JsonObject jsonobject){
		elements.add(jsonobject);
	}
	public void put(JsonArray jsonarray){
		elements.add(jsonarray);
	}
	public void put(JsonNumber jsonNum){
		elements.add(jsonNum.toString());
	}
	
	public String toString(){
		return toString(0,false);
	}
	
	public String toString(int decal, boolean structure){
		StringBuilder sb = new StringBuilder();
		sb.append('[');
		for (int i = 0; i < elements.size(); i++) {
			if(elements.get(i) instanceof JsonObject){
				if(structure)sb.append(decal(decal+1));
				sb.append(((JsonObject)elements.get(i)).toString(decal+1, structure));
			}
			if(getcls(elements.get(i)) == JsonArray.class){
				if(structure)sb.append(decal(decal+1));
				sb.append(((JsonArray)elements.get(i)).toString(decal+1, structure));
			}
			if(getcls(elements.get(i)) == String.class){
				if(structure)sb.append(decal(decal+1));
				String s = (String) elements.get(i);
				if (s.charAt(0)=='"'){
					sb.append(s.substring(1, s.length()-1).replaceAll("\\\\", "\\\\\\\\").replaceAll("\"", "\\\\\""));
				}else{
					sb.append(s);
				}
			}
			if(getcls(elements.get(i)) == JsonNumber.class){
				if(structure)sb.append(decal(decal+1));
				sb.append((JsonNumber)elements.get(i));
			}
			if(i != elements.size()-1){
				sb.append(',');
			}else{
				if(structure)sb.append(decal(decal));
			}
		}
		sb.append(']');
		return sb.toString();
	}
	
	private String decal(int decal){
		StringBuilder sb = new StringBuilder();
		sb.append('\n');
		for (int i = 0; i < decal; i++) {
			sb.append("    ");
		}
		return sb.toString();
	}
	
	public int size(){
		return elements.size();
	}
	
	public Class getcls(int index){
		return getcls(elements.get(index));
	}
	
	public Class getcls(Object element){
		if(element instanceof String) return String.class;
		if(element instanceof JsonArray) return JsonArray.class;
		if(element instanceof JsonObject) return JsonObject.class;
		if(element instanceof JsonNumber) return JsonNumber.class;
		return null;
	}
	
	public <T> T get(int index) throws ClassCastException{
		return (T) elements.get(index);
	}
	
	public Object get(String... path){
		int i = new Integer(path[0]);
		if(i<elements.size()){
			Object element = get(i); 
			if(getcls(element) == String.class){
				String r = get(i);
				if(r.charAt(0)=='"' && r.charAt(r.length()-1)=='"'){
					return r.subSequence(1, r.length()-1);
				}
				else if(JsonNumber.isnumber(r)){
					return new JsonNumber(r);
				}
				else if(r.equals("true")) return true;
				else if(r.equals("false")) return false;
				else return null;
			}
			if(getcls(element) == JsonArray.class){
				JsonArray s = get(i);
				String[] path2 = Arrays.copyOfRange(path, 1, path.length);
				return (path.length>1)?s.get(path2):s;
			}
			if(getcls(element) == JsonObject.class){
				JsonObject s = get(i);
				String[] path2 = Arrays.copyOfRange(path, 1, path.length);
				return (path.length>1)?s.get(path2):s;
			}
		}
		return null;
	}
}
