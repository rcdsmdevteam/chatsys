package fr.orandjadev.MJson;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import javax.net.ssl.HttpsURLConnection;

public class JsonWeb {

	public static String getContent(String jsonURL) {
		String content = "";
		  
	 	try {
			URL url = new URL(jsonURL);
			HttpURLConnection con;
			if(jsonURL.startsWith("https")) {
				con = (HttpsURLConnection) url.openConnection();
			} else {
				con = (HttpURLConnection) url.openConnection();
			}
			if(con!=null){
				BufferedReader br = new BufferedReader(new InputStreamReader(con.getInputStream()));
				String input;
				while ((input = br.readLine()) != null){
					content += input;
				}
				br.close();
			}
	 	} catch (Exception e) {
	 	   e.printStackTrace();
	 	   return "";
	 	}
		return content;
	}
}
