package fr.orandjadev.MJson;

public class JsonReader {
	
	public static JsonObject ReadJson(String JsonString){
		String key = null;
		String value = null;
		boolean keyorvalue = false;
		int number=0;
		JsonObject rJson = new JsonObject();
		
		for (int p = 0; p < JsonString.length(); p++) {
			char c = JsonString.charAt(p);
			switch (c){
				case '"':
					number=0;
					if(!keyorvalue){
						Object a[] = toNextMark(JsonString.substring(p));
						key =(String) a[1];
						p+=(int)a[0]+1;
					}else{
						Object a[] = toNextMark(JsonString.substring(p));
						value =(String) a[1];
						p+=(int)a[0]+1;
						rJson.put(key,value);
						key = null;
						value = null;
					}
					break;
				case ':':
					keyorvalue=true;
					number=p;
					break;
				case '{':
					number=0;
					String sub = toNextcurlybrace(JsonString.substring(p));
					p+=sub.length();
					if (key==null){
						return ReadJson(sub.substring(1));
					}
					else{
						rJson.put(key, ReadJson(sub.substring(1)));
						key = null;
					}break;
				case '[':
					number=0;
					String subb = toNextsquarebracket(JsonString.substring(p));
					p+=subb.length();
					if (key!=null){
						rJson.put(key,ReadArray(subb.substring(1, subb.length())));
						key = null;
					}break;
					

				case '}':

				case ']':

				case ',':
					keyorvalue=false;
					if(number != 0){
						String s = JsonString.substring(number+1, p).replaceAll("\\s", "");
						if(s.equals("true") ){
							rJson.put(key, true);
						}else if(s.equals("false") ){
							rJson.put(key, false);
						}else if(s.equals("null") ){
							rJson.put(key);
						}else rJson.put(key,new JsonNumber(s));
					}
					break;
			}
		}
		if(number != 0){
			String s = JsonString.substring(number+1).replaceAll("\\s", "");
			if(s.equals("true") ){
				rJson.put(key, true);
			}else if(s.equals("false") ){
				rJson.put(key, false);
			}else if(s.equals("null") ){
				rJson.put(key);
			}else{
				rJson.put(key,new JsonNumber(s));
			}
		}
		return rJson;
	}
	
	@SuppressWarnings("unused")
	private static boolean nextIsComma(String s){
		for (int i = 1; i < s.length(); i++) {
			if(s.charAt(i) == ',')
				return true;
			else if(s.charAt(i) == '"') return false;
		}
		return false;
	}
	
	@SuppressWarnings("unused")
	private static String toNextComma(String s){
		for (int i = 1; i < s.length(); i++) {
			if(s.charAt(i) == ',')
				return s.substring(0,i);
		}
		return s;
	}
	
	private static Object[] toNextMark(String s){
		int backslashs=0;
		int cursorbackslash;
		int r =0;
		for (int i = 1; i < s.length(); i++) {
			if(s.charAt(i) == '"'){
				backslashs=0;
				cursorbackslash = i-1;
				while(cursorbackslash>0 && s.charAt(cursorbackslash) == '\\' ){
					backslashs++;
					cursorbackslash--;
				}
				if(backslashs%2==0){
					s=s.substring(1, i);
					return new Object[]{s.length()+r,s.replaceAll("\\\\\\\\", "\\\\")};
				}else {
					StringBuffer sb  = new StringBuffer(s);
					sb.deleteCharAt(cursorbackslash+1);
					s = sb.toString();
					r++;
				}
			}
		}
		
		return new Object[]{s.length(),s.substring(1)};
	}
	

	
	private static String toNextcurlybrace(String s){
		int count=1;
		int countcomma=0;
		for (int i = 1; i < s.length(); i++) {
			char c = s.charAt(i);
			if (c=='"')countcomma++;
			if(countcomma%2==0){
				switch(c){
					case '{':
						count++;
						break;
					case '}':
						count--;
						break;
				}
			}
			if(count==0)return s.substring(0, i);
		}
		return s;
	}
	private static String toNextsquarebracket(String s){
		int count=1;
		int countcomma=0;
		for (int i = 1; i < s.length(); i++) {
			char c = s.charAt(i);
			if (c=='"')countcomma++;
			if(countcomma%2==0){
				switch(c){
					case '[':
						count++;
						break;
					case ']':
						count--;
						break;
				}
			}
			if(count==0){
				return s.substring(0, i);
			}
		}
		return s;
	}
	
	public static JsonArray ReadArray(String JsonArray){
		
		JsonArray JA = new JsonArray();
		int sub =0;
		boolean number=true;
		for (int p = 0; p < JsonArray.length(); p++) {
			char c = JsonArray.charAt(p);
			switch (c) {
				case ',':
					if (number){
						String value = JsonArray.substring(sub, p);
						if (value.equals("null"))JA.put();
						else if (value.equals("true"))JA.put(true);
						else if (value.equals("false"))JA.put(false);
						else JA.put(new JsonNumber(value));
					}
					sub = p+1;
					number=true;
					break;
				case '{':
					number=false;
					String jsonObj = toNextcurlybrace(JsonArray.substring(p));
					JA.put(ReadJson(jsonObj));
					p+=jsonObj.length();
					break;
				case '[':
					number=false;
					String Array = toNextsquarebracket(JsonArray.substring(p));
					JA.put(ReadArray(Array.substring(1)));
					p+=Array.length();
					break;
				case '"':
					number=false;
					Object a[] = toNextMark(JsonArray.substring(p));
					JA.put((String) a[1]);
					p+=(int)a[0]+1;
					break;
			}
		}

		if (number){
			String value = JsonArray.substring(sub, JsonArray.length());
			if (!value.equals("") && !value.matches("/^\\s*$/")){
				if (value.equals("null"))JA.put();
				else if (value.equals("true"))JA.put(true);
				else if (value.equals("false"))JA.put(false);
				else JA.put(new JsonNumber(value));
			}
		}
		return JA;
	}
}
